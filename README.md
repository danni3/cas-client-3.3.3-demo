# java cas client 3.3.3 demo

* 本客户端demo使用的cas-client-core.jar版本：3.3.3
* 如果需要看core的源代码：https://github.com/apereo/java-cas-client/releases
* cas服务端4.0.0测试通过。包括：
    - 登录
    - 登出
* tomcat版本：9.0.12测试OK

## 登出

* 需要注意，需要先重定向到logoutsuccess，如果直接跳到主页，会有时候失败，要刷新页面才行。
